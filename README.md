# README #

## ESP32 Server ##

* Code to create a Wi-Fi server on the ESP32
* Includes serial communication to communicate with the Python GUI
* Includes ability to save Wi-Fi settings to flash memory for quick startup

## Set up ##

* Compile and upload to an ESP32 using Arduino IDE